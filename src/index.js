const OrgChart = require("@balkangraph/orgchart.js");
import {dataPath} from "./config";


OrgChart.templates.ana.field_0 =
    '<text class="field_0" style="font-size: 20px;" fill="#ffffff" x="125" y="30" text-anchor="middle">{val}</text>';
OrgChart.templates.ana.field_1 =
    '<text class="field_1" style="font-size: 14px;" fill="#ffffff" x="125" y="50" text-anchor="middle">{val}</text>';
OrgChart.templates.ana.field_2 =
    '<text class="field_2" style="font-size: 14px; font-weight: bold" fill="#ffffff" x="125" y="70" text-anchor="middle">${val}</text>';
OrgChart.templates.ana.field_3 =
    '<text class="field_3" style="font-size: 14px;" fill="#ffffff" x="125" y="90" text-anchor="middle">{val}</text>';

fetch(dataPath).then(
    d => d.json()
).then(
    d => {
        let chart = new OrgChart(document.getElementById("ORGAN"), {
            mode: 'light',
            enableSearch: true,
            mouseScrool: OrgChart.action.none,
            nodeMouseClick: OrgChart.action.none,
            nodeBinding: {
                field_0: "name",
                field_1: "title",
                field_2: "salary"
            },
            nodes: d
        });
    }
)

