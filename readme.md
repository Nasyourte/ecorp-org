# ECORP ORG CHART
On veut afficher l'organigramme de notre entre prise sur une page web.

La page web est faite.

## Employés

Télécharger et importer : https://github.com/datacharmer/test_db
> la structure ET les données.

## Front
Pour lancer la page web. 🚀

Dans le clone de ce repo :
```shell
npm install
npm run start
```
## DATA
Pour fonctionner la page a besoin des données des employés **en JSON**.

Par exemple :
```json
[
  { "id": 1,
    "name": "Amber McKenzie",
    "title": "CEO",
    "salary": 1000.2
  },
  { "id": 2,
    "pid": 1,
    "title": "Sales manager",
    "name": "Ava Field",
    "salary": 500.99
  },
  { "id": 3,
    "pid": 1,
    "title": "sushi caretaker",
    "name": "Rhys Harper",
    "salary": 150.2
  }
]
```

Un tableau qui contient des objets (dictionnaires) avec :
- **id** : int ; un numéro unique
- **pid** : int (optionel le chef chef n'a pas de chef) ; l'id de son chef / N+1 / manager.
- **name** : str ; le nom complet de l'employé.
- **title** : str ; son titre honorifique dans l'entreprise.
- **salary**: float ; son salaire.

Pour "connecter" la page aux données vous devez indiquer l'url de votre JSON dans la variable `dataPath` du fichier `config.js`.

> dans l'exemple on utilise `bloup.json` mais ca peut être n'importe quoi. Un fichier ou... la route d'une API...
> Si vous avez des problèmes d'url de fichier, mettez le dans `static` et mettez juste son nom de fichier dans `config.js`.
